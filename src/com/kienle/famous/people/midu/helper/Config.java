package com.kienle.famous.people.midu.helper;

import android.os.Environment;

public class Config {
    public static final String DATA_ZIP_FILE = "data.zip";
    public static final String APP_FOLDER = Environment.getExternalStorageDirectory().getPath() + "/Midu";
    public static final String FOLDER_IMAGE = APP_FOLDER + "/midu";
}
